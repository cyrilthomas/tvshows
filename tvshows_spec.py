import unittest
import json
from tvshows import app

class TestCases(unittest.TestCase):
    def correct_input_data(self):
        return r'''
            {
              "payload": [
                {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": true,
                  "episodeCount": 3,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                }
              ]
            }
        '''

    def wrong_payload_key(self):
        return r'''
            {
              "load": [
                {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": true,
                  "episodeCount": 3,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                }
              ]
            }
        '''

    def wrong_payload_structure(self):
        return r'''
            {
              "payload": {
                "data": {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": true,
                  "episodeCount": 3,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                }
              }
            }
        '''

    def empty_payload_list(self):
        return '''
            {
              "payload": []
            }
        '''

    def empty_payload_dict(self):
        return '''
            {
              "payload": {}
            }
        '''

    def empty_payload_string(self):
        return '''
            {
              "payload": ''
            }
        '''

    def drm_false(self):
        return r'''
            {
              "payload": [
                {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": false,
                  "episodeCount": 3,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                },
                {
                  "country": " USA",
                  "description": "The Taste puts 16 culinary competitors in the kitchen, where four of the World's most notable culinary masters of the food world judges their creations based on a blind taste. Join judges Anthony Bourdain, Nigella Lawson, Ludovic Lefebvre and Brian Malarkey in this pressure-packed contest where a single spoonful can catapult a contender to the top or send them packing.",
                  "drm": true,
                  "episodeCount": 2,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": {
                    "channel": null,
                    "channelLogo": "http://catchup.ninemsn.com.au/img/player/logo_go.gif",
                    "date": null,
                    "html": "<br><span class=\"visit\">Visit the Official Website</span></span>",
                    "url": "http://go.ninemsn.com.au/"
                  },
                  "primaryColour": "#df0000",
                  "seasons": [
                    {
                      "slug": "show/thetaste/season/1"
                    }
                  ],
                  "slug": "show/thetaste",
                  "title": "The Taste",
                  "tvChannel": "GEM"
                }
              ]
            }
        '''

    def episode_less_than_one(self):
        return r'''
            {
              "payload": [
                {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": true,
                  "episodeCount": 0,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                },
                {
                  "country": " USA",
                  "description": "The Taste puts 16 culinary competitors in the kitchen, where four of the World's most notable culinary masters of the food world judges their creations based on a blind taste. Join judges Anthony Bourdain, Nigella Lawson, Ludovic Lefebvre and Brian Malarkey in this pressure-packed contest where a single spoonful can catapult a contender to the top or send them packing.",
                  "drm": true,
                  "episodeCount": 2,
                  "genre": "Reality",
                  "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg"
                  },
                  "language": "English",
                  "nextEpisode": {
                    "channel": null,
                    "channelLogo": "http://catchup.ninemsn.com.au/img/player/logo_go.gif",
                    "date": null,
                    "html": "<br><span class=\"visit\">Visit the Official Website</span></span>",
                    "url": "http://go.ninemsn.com.au/"
                  },
                  "primaryColour": "#df0000",
                  "seasons": [
                    {
                      "slug": "show/thetaste/season/1"
                    }
                  ],
                  "slug": "show/thetaste",
                  "title": "The Taste",
                  "tvChannel": "GEM"
                }
              ]
            }
        '''

    def missing_key(self):
        return r'''
            {
              "payload": [
                {
                  "country": "UK",
                  "description": "What's life like when you have enough children to field your own football team?",
                  "drm": true,
                  "episodeCount": 1,
                  "genre": "Reality",
                  "image": {
                  },
                  "language": "English",
                  "nextEpisode": null,
                  "primaryColour": "#ff7800",
                  "seasons": [
                    {
                      "slug": "show/16kidsandcounting/season/1"
                    }
                  ],
                  "slug": "show/16kidsandcounting",
                  "title": "16 Kids and Counting",
                  "tvChannel": "GEM"
                }
              ]
            }
        '''

    def corrupt_data(self):
        return r'''
            [xyz]
        '''

    def test_television_shows(self):
        self.test_app = app.test_client()
        res = self.test_app.post('/', data=self.correct_input_data())
        self.assertEquals(res.status, "200 OK")
        output = json.loads(res.data)
        self.assertEquals(output['response'][0]['image'], 'http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg')
        self.assertEquals(output['response'][0]['slug'], 'show/16kidsandcounting')
        self.assertEquals(output['response'][0]['title'], '16 Kids and Counting')

        res = self.test_app.post('/', data=self.wrong_payload_key())
        self.assertEquals(res.status, "400 BAD REQUEST")
        output = json.loads(res.data)
        self.assertEquals(output['error'], 'Could not decode request: JSON parsing failed')

        res = self.test_app.post('/', data=self.wrong_payload_structure())
        self.assertEquals(res.status, "400 BAD REQUEST")
        output = json.loads(res.data)
        self.assertEquals(output['error'], 'Could not decode request: JSON parsing failed')

        res = self.test_app.post('/', data=self.empty_payload_list())
        self.assertEquals(res.status, "200 OK")
        output = json.loads(res.data)
        self.assertEquals(output['response'], [])

        res = self.test_app.post('/', data=self.empty_payload_dict())
        self.assertEquals(res.status, "400 BAD REQUEST")
        output = json.loads(res.data)
        self.assertEquals(output['error'], 'Could not decode request: JSON parsing failed')

        res = self.test_app.post('/', data=self.empty_payload_string())
        self.assertEquals(res.status, "400 BAD REQUEST")
        output = json.loads(res.data)
        self.assertEquals(output['error'], 'Could not decode request: JSON parsing failed')

        res = self.test_app.post('/', data=self.drm_false())
        self.assertEquals(res.status, "200 OK")
        output = json.loads(res.data)
        self.assertEquals(len(output['response']), 1)

        res = self.test_app.post('/', data=self.episode_less_than_one())
        self.assertEquals(res.status, "200 OK")
        output = json.loads(res.data)
        self.assertEquals(len(output['response']), 1)
        self.assertEquals(output['response'][0]['image'], 'http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg')
        self.assertEquals(output['response'][0]['slug'], 'show/thetaste')
        self.assertEquals(output['response'][0]['title'], 'The Taste')

        res = self.test_app.post('/', data=self.missing_key())
        self.assertEquals(res.status, "200 OK")
        output = json.loads(res.data)
        self.assertEquals(output['response'], [])

        res = self.test_app.post('/', data=self.corrupt_data())
        self.assertEquals(res.status, "400 BAD REQUEST")
        output = json.loads(res.data)
        self.assertEquals(output['error'], 'Could not decode request: JSON parsing failed')

if __name__ == '__main__':
    unittest.main()