# TV Shows - DRM Validation API

This API allows you to `POST` JSON data in the below format to the hosted URL http://powerful-dawn-7885.herokuapp.com/

```json
{
    "payload": [
        {
            "country": "UK",
            "description": "What's life like when you have enough children to field your own football team?",
            "drm": true,
            "episodeCount": 3,
            "genre": "Reality",
            "image": {
                "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
            },
            "language": "English",
            "nextEpisode": null,
            "primaryColour": "#ff7800",
            "seasons": [
                {
                    "slug": "show/16kidsandcounting/season/1"
                }
            ],
            "slug": "show/16kidsandcounting",
            "title": "16 Kids and Counting",
            "tvChannel": "GEM"
        }
    ]
}
```

And filter the TV shows which have DRM and a minimum of 1 episode in a JSON response format

```json
{
    "response": [
        {
            "image": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg",
            "slug": "show/16kidsandcounting",
            "title": "16 Kids and Counting"
        }
     ]
}
```


Incase of an invalid input you will be returned a 400 error and a JSON response
```json
{ 
    "error": "Could not decode request: JSON parsing failed"
}
```

You can use a tool like POSTMAN REST chrome app to post JSON for testing 
https://chrome.google.com/webstore/detail/postman-rest-client-packa/fhbjgbiflinjbdggehcddcbncdddomop?utm_source=chrome-app-launcher

Or invoke it via you application code.


# Running the app locally

Download a copy of this repo

Install the below requirements
* Python 2.7;
* Flask 0.10.1 (pip install flask)

Run the below code in a terminal
* cd tvshows;
* python tvshows.py

Use POSTMAN REST Client to interact with the application