from flask import Flask, request, jsonify
import json

app = Flask(__name__)

class InvalidInputRequest(Exception):
    pass

@app.errorhandler(InvalidInputRequest)
def bad_input_request(error):
    """
    Generic error response for all invalid input request
    """
    return jsonify(**{ 
            'error': 'Could not decode request: JSON parsing failed' 
        }), 400

@app.route('/', methods=['GET', 'POST'])
def television_shows():
    """
    ================================================================================
    Required format of the JSON input
    ================================================================================
    {
        "payload": [
            {
                "drm": true,
                "episodeCount": 3,
                "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                },
                "slug": "show/16kidsandcounting",
                "title": "16 Kids and Counting"
            }
        ]
    }

    Requests which does not follow this format would receive an 400 HTTP code along with a JSON error response
    
    ================================================================================
    Minimal format of the JSON input
    ================================================================================
    {
        "payload": []
    }

    The service would just return an empty response if payload is an empty list 
    or if none of the payload data matches the conditions (drm == True and episodeCount >0)
    {
        "response" : []
    }

    ================================================================================
    Invalid inputs
    ================================================================================
    {
        "load": [
            {
                "drm": true,
                "episodeCount": 3,
                "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                },
                "slug": "show/16kidsandcounting",
                "title": "16 Kids and Counting"
            }
        ]
    }
    --------------------------------------------------------------------------------
    {
        "payload": { "data" : {
                "drm": true,
                "episodeCount": 3,
                "image": {
                    "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                },
                "slug": "show/16kidsandcounting",
                "title": "16 Kids and Counting"
            }
        }
    }

    ================================================================================
    Output response
    ================================================================================
    {
        "response": [
            {
                "image": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg", 
                "slug": "show/16kidsandcounting", 
                "title": "16 Kids and Counting"
            }
        ]
    }
    """
    if request.method == 'GET':
        # HTML landing page
        import re
        doc = re.sub(r'\s', '&nbsp', re.sub(r'\n', '<br/>', television_shows.__doc__))
        return 'Usage<br/>Post request to URL - {0}<br/>{1}'.format(request.base_url, doc)

    response = {}
    result = []
    try:
        series_data = json.loads(request.data)
        series_data['payload'][:]
    except (ValueError, KeyError, TypeError):
        raise InvalidInputRequest
    else:
        for series in series_data['payload']:
            try:
                if series['drm'] and series['episodeCount'] > 0:
                    result.append({
                        'image': series['image']['showImage'], 'slug': series['slug'], 'title': series['title']
                    })
            except KeyError:
                pass
    return jsonify(**{ 'response': result })

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
